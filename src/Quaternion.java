import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private double real;
    private double imagI;
    private double imagJ;
    private double imagK;
    private final int PRECISION = 5;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        real = a;
        imagI = b;
        imagJ = c;
        imagK = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return real;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return imagI;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return imagJ;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return imagK;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String realTest = Double.toString(real);
        String iWithPlus = Double.toString(imagI);
        String jWithPlus = Double.toString(imagJ);
        String kWithPlus = Double.toString(imagK);
        if (imagI >= 0) {
            iWithPlus = "+" + imagI;
        }
        if (imagJ >= 0) {
            jWithPlus = "+" + imagJ;
        }
        if (imagK >= 0) {
            kWithPlus = "+" + imagK;
        }
        String answer = String.format("%1$s%2$si%3$sj%4$sk", realTest, iWithPlus, jWithPlus, kWithPlus);
        return answer;
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        Pattern p = Pattern.compile("-*\\d+[.]?\\d*[ijk]?"); // Double matcher https://stackoverflow.com/questions/16817031/how-to-iterate-over-regex-expression/16817458 insipratsioon.
        Matcher m = p.matcher(s);
        Double[] elements = new Double[4];
        Integer position = 0;
        while (m.find()) {
            String element = m.group();
            if (element.endsWith("i") && elements[1] == null){
                elements[1] = Double.valueOf(element.substring(0, element.length() - 1));
            }
            else if (element.endsWith("j") && elements[2] == null){
                elements[2] = Double.valueOf(element.substring(0, element.length() - 1));
            }
            else if (element.endsWith("k") && elements[3] == null){
                elements[3] = Double.valueOf(element.substring(0, element.length() - 1));
            } else {
                elements[0] = Double.valueOf(element);
            }
        }

        for (int i = 0; i < 4; i++) {
            if (elements[i] == null) {
                elements[i] = 0.0;
            }
        }

        return new Quaternion(elements[0], elements[1], elements[2], elements[3]);
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(real, imagI, imagJ, imagK);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return areEqualDouble(real, 0.0, PRECISION) &&
                areEqualDouble(imagI, 0.0, PRECISION) &&
                areEqualDouble(imagJ, 0.0, PRECISION) &&
                areEqualDouble(imagK, 0.0, PRECISION);
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(real, 0 - imagI, 0 - imagJ, 0 - imagK);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(0 - real, 0 - imagI, 0 - imagJ, 0 - imagK);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(real + q.real, imagI + q.imagI, imagJ + q.imagJ, imagK + q.imagK);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        return new Quaternion(
                (real * q.real) - (imagI * q.imagI) - (imagJ * q.imagJ) - (imagK * q.imagK),
                (real * q.imagI) + (imagI * q.real) + (imagJ * q.imagK) - (imagK * q.imagJ),
                (real * q.imagJ) - (imagI * q.imagK) + (imagJ * q.real) + (imagK * q.imagI),
                (real * q.imagK) + (imagI * q.imagJ) - (imagJ * q.imagI) + (imagK * q.real)
        );
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(real * r, imagI * r, imagJ * r, imagK * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     *              ((-b)/(a*a+b*b+c*c+d*d))i +
     *              ((-c)/(a*a+b*b+c*c+d*d))j +
     *              ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (isZero()) {
            throw new RuntimeException("You can't inverse a quaternion which has a value of 0: " + toString());
        }
        return new Quaternion(
                real / ((real * real) + (imagI * imagI) + (imagJ * imagJ) + (imagK * imagK)),
                (0 - imagI) / ((real * real) + (imagI * imagI) + (imagJ * imagJ) + (imagK * imagK)),
                (0 - imagJ) / ((real * real) + (imagI * imagI) + (imagJ * imagJ) + (imagK * imagK)),
                (0 - imagK) / ((real * real) + (imagI * imagI) + (imagJ * imagJ) + (imagK * imagK))
        );
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return plus(q.opposite());
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("The given quaternion is zero: " + q.toString());
        }
        Quaternion inverse = q.inverse();
        if (!isZero()) {
            return times(inverse);
        }
        throw new RuntimeException("Division by zero.");
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("The given quaternion is zero: " + q.toString());
        }
        Quaternion inverse = q.inverse();
        if (!isZero()) {
            return inverse.times(this);
        }
        throw new RuntimeException("Division by zero.");
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if (this == qo) {
            return true;
        }
        if (qo instanceof Quaternion) {
            Quaternion other = (Quaternion) qo;

            return areEqualDouble(real, other.real, 5) &&
                    areEqualDouble(imagI, other.imagI, 5) &&
                    areEqualDouble(imagJ, other.imagJ, 5) &&
                    areEqualDouble(imagK, other.imagK, 5);
        }
        return false;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */

    public Quaternion dotMult(Quaternion q) {
        return times(q.conjugate()).plus(q.times(conjugate())).times(0.5);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(real, imagI, imagJ, imagK);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(real * real + imagI * imagI + imagJ * imagJ + imagK * imagK);
    }

    // Code by StackOverflow user carlosvin: https://stackoverflow.com/questions/356807/java-double-comparison-epsilon
    public static boolean areEqualDouble(double a, double b, int precision) {
        return Math.abs(a - b) <= Math.pow(10, -precision);
    }


    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
//        Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
//        if (arg.length > 0)
//            arv1 = valueOf(arg[0]);
//        System.out.println("first: " + arv1.toString());
//        System.out.println("real: " + arv1.getRpart());
//        System.out.println("imagi: " + arv1.getIpart());
//        System.out.println("imagj: " + arv1.getJpart());
//        System.out.println("imagk: " + arv1.getKpart());
//        System.out.println("isZero: " + arv1.isZero());
//        System.out.println("conjugate: " + arv1.conjugate());
//        System.out.println("opposite: " + arv1.opposite());
//        System.out.println("hashCode: " + arv1.hashCode());
//        Quaternion res = null;
//        try {
//            res = (Quaternion) arv1.clone();
//        } catch (CloneNotSupportedException e) {
//        }
//        ;
//        System.out.println("clone equals to original: " + res.equals(arv1));
//        System.out.println("clone is not the same object: " + (res != arv1));
//        System.out.println("hashCode: " + res.hashCode());
//        res = valueOf(arv1.toString());
//        System.out.println("string conversion equals to original: "
//                + res.equals(arv1));
//        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
//        if (arg.length > 1)
//            arv2 = valueOf(arg[1]);
//        System.out.println("second: " + arv2.toString());
//        System.out.println("hashCode: " + arv2.hashCode());
//        System.out.println("equals: " + arv1.equals(arv2));
//        res = arv1.plus(arv2);
//        System.out.println("plus: " + res);
//        System.out.println("times: " + arv1.times(arv2));
//        System.out.println("minus: " + arv1.minus(arv2));
//        double mm = arv1.norm();
//        System.out.println("norm: " + mm);
//        System.out.println("inverse: " + arv1.inverse());
//        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
//        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
//        System.out.println("dotMult: " + arv1.dotMult(arv2));

//        Quaternion q1 = Quaternion.valueOf("1.0+2.0i+3.0j+5.0k");
//        Quaternion q2 = Quaternion.valueOf("4.0+7.0i+-1.0j+-6.0k");
//        System.out.println(q1.dotMult(q2));

//        Quaternion q1 = Quaternion.valueOf("0.25+0.25i+-0.75j+0.5k");
//        Quaternion q2 = Quaternion.valueOf("1.0+-2.0i+-1.0j+2.0k");
//        System.out.println(q1.divideByRight(q2));

//        Quaternion toInverse = Quaternion.valueOf("0.0032+-0.0048i+-0.0096j+-0.0384k");
//        System.out.println(toInverse.inverse()
        System.out.println(Quaternion.valueOf("1+2i+3j+1k"));
    }
}